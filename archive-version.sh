#!/bin/sh

set -e

echo " "
echo " "
echo 'build kafka start'

set '3.3.0'
SCALA_VERSION=2.13
KAFKA_DISTRO_BASE_URL=https://archive.apache.org/dist/kafka/

for i in "$@"; do
    KAFKA_VERSION=$i

    echo "build kafka:${SCALA_VERSION}-${KAFKA_VERSION} start"

    docker build \
        --build-arg scala_version=${SCALA_VERSION} \
        --build-arg kafka_version=${KAFKA_VERSION} \
        --build-arg kafka_distro_base_url=${KAFKA_DISTRO_BASE_URL} \
        -t kafka:${SCALA_VERSION}-${KAFKA_VERSION}-${TAG_VERSION} \
        -f ${CI_PROJECT_DIR}/Dockerfile . --no-cache
    docker image tag kafka:${SCALA_VERSION}-${KAFKA_VERSION}-${TAG_VERSION} ${CI_REGISTRY}/opcal-project/containers/kafka:${SCALA_VERSION}-${KAFKA_VERSION}
    docker push ${CI_REGISTRY}/opcal-project/containers/kafka:${SCALA_VERSION}-${KAFKA_VERSION}

    echo "build kafka:${SCALA_VERSION}-${KAFKA_VERSION} finished"
done

echo 'build kafka finished'
echo " "
echo " "